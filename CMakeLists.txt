cmake_minimum_required(VERSION 3.19)
project(Beleg)

set(CMAKE_CXX_STANDARD 14)

# include stuff
find_package(OpenGL REQUIRED)

include_directories(
        ext/GLAD/include/
        ext/GLFW/include/
        ext/GLM/glm/
)

link_directories(
        ext/GLFW/lib/
)

set(ALL_LIBS
        ${OPENGL_LIBRARY}
        glfw3
)

set(ALL_FILES
        src/main.cpp ext/GLAD/src/glad.c

        src/sceneobjects/object.cpp src/sceneobjects/object.h
        src/sceneobjects/objectTransform.cpp src/sceneobjects/objectTransform.h
        src/sceneobjects/scene.cpp src/sceneobjects/scene.h
        src/sceneobjects/camera.cpp src/sceneobjects/camera.h
        src/sceneobjects/light.cpp src/sceneobjects/light.h

        src/shader/initializeScene.cpp src/shader/initializeScene.h
        src/shader/raymarchingSetup.cpp src/shader/raymarchingSetup.h
        src/shader/shaderUtility.cpp src/shader/shaderUtility.h
        src/shader/cpp/raycast.cpp src/shader/cpp/raycast.h src/shader/cpp/distanceFunctions.cpp src/shader/cpp/distanceFunctions.h src/sceneobjects/objectMaterial.cpp src/sceneobjects/objectMaterial.h src/sceneobjects/shaderPassable.cpp src/sceneobjects/shaderPassable.h)

add_executable(Beleg ${ALL_FILES})
target_link_libraries(Beleg ${ALL_LIBS})


