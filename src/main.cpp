#include "shader/shaderUtility.h"
#include "shader/raymarchingSetup.h"
#include "shader/cpp/raycast.h"
#include <iostream>

#include <GLFW/glfw3.h>

int screenWidth = 800;
int screenHeight = 600;

bool advanceBudgetTime = true;

void processKeyboardInput(GLFWwindow *window, int key, int scancode, int action, int mods) {
    if(key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);

    if(key == GLFW_KEY_SPACE && action == GLFW_PRESS)
        advanceBudgetTime = false;

    if(key == GLFW_KEY_SPACE && action == GLFW_RELEASE)
        advanceBudgetTime = true;
}

void processMouseInput(GLFWwindow* window, int button, int action, int mods) {
    if(button == GLFW_MOUSE_BUTTON_1 && action == GLFW_PRESS) {
        double posX, posY;
        glfwGetCursorPos(window, &posX, &posY);

        // remap the range from 0 to width or height into -1 to 1
        // (-2 in the second part is wanted, to flip the coordinates)
        glm::vec2 cursor = glm::vec2(posX / screenWidth * 2 - 1, posY / screenHeight * - 2 + 1);
        raycastResult result = raycast(getCurrentScene(), cursor);

        if(result.hitSomething) {
            std::cout << "Hit registered [X: " << result.hitPosition.x << ", Y: " << result.hitPosition.y << ", Z: " << result.hitPosition.z << "]" << std::endl;
        }
        else std::cout << "Nope" << std::endl;
    }
}

void mainLoop(GLFWwindow* window) {

    setupShader(screenWidth, screenHeight);
    glfwSetKeyCallback(window, processKeyboardInput);
    glfwSetMouseButtonCallback(window, processMouseInput);

    while(!glfwWindowShouldClose(window)) {
        runRaymarchingShader(screenWidth, screenHeight, advanceBudgetTime);
        glfwSwapBuffers(window);
        glfwPollEvents();
    }
}

void framebuffer_size_callback(GLFWwindow* window, int newWidth, int newHeight) {
    glViewport(0, 0, newWidth, newHeight);
    std::cout << "Resized to " << std::endl;
    screenWidth = newWidth;
    screenHeight = newHeight;
}

int main(){

    // Init GLFW
    if(!glfwInit()) {
        std::cout << "Failed to initialize glfw" << std::endl;
    }

    GLFWwindow* window = glfwCreateWindow(screenWidth, screenHeight, "Raymarching", NULL, NULL);
    if (window == NULL) {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);

    // Load all OpenGL functions using the glfw loader function
    // If you use SDL you can use: https://wiki.libsdl.org/SDL_GL_GetProcAddress
    if (!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress))
        throw std::runtime_error("Could not initialize GLAD!");
    glGetError(); // pull and ignore unhandled errors like GL_INVALID_ENUM

    // glad populates global constants after loading to indicate,
    // if a certain extension/version is available.
    printf("OpenGL %d.%d\n", GLVersion.major, GLVersion.minor);

    // Take care of setting up the window and resizing
    glViewport(0, 0, 800, 600);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

    mainLoop(window);

    glfwTerminate();
    return 0;
}



