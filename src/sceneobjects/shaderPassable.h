#ifndef BELEG_SHADERPASSABLE_H
#define BELEG_SHADERPASSABLE_H


#include <glad/glad.h>
#include <unordered_map>
#include <string>

class shaderPassable {
protected:
    std::unordered_map<int, std::unordered_map<std::string, GLint>> uniformLocationCache;

    shaderPassable();

    GLint getUniformLocation(int programId, const std::string& identifier);

};


#endif
