#ifndef BELEG_OBJECTTRANSFORM_H
#define BELEG_OBJECTTRANSFORM_H

#include <string>
#include <vec3.hpp>
#include <mat4x4.hpp>

class objectTransform {

public :
    glm::vec3 position;
    glm::vec3 rotation;
    glm::vec3 scale;

    objectTransform();
    objectTransform(glm::vec3 position);
    objectTransform(glm::vec3 position, glm::vec3 rotation);
    objectTransform(glm::vec3 position, glm::vec3 rotation, glm::vec3 scale);

    glm::mat4x4 calculateMatrix();

};


#endif
