#ifndef BELEG_LIGHT_H
#define BELEG_LIGHT_H


#include <string>
#include <vec3.hpp>
#include "shaderPassable.h"

class light : shaderPassable {
public:
    glm::vec3 position;
    glm::vec3 color;

    light(glm::vec3 position);
    light(glm::vec3 position, glm::vec3 color);

    void passToShader(int programId, const std::string& identifier);
};


#endif