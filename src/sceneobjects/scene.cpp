#include <glad/glad.h>
#include "scene.h"

scene::scene() : mainCamera(), actors(), mainLight(glm::vec3(0,1,0)) {}

scene::scene(camera mainCamera, light mainLight) :
    shaderPassable(), mainCamera(mainCamera), actors(), mainLight(mainLight){}

void scene::passToShader(int programId) {

    GLuint actorCountLoc = getUniformLocation(programId, "actorCount");
    glUniform1f(actorCountLoc, actors.size());

    std::list<object>::iterator it;
    int index = 0;
    for(it = actors.begin(); it != actors.end(); it++, index++) {
        std::string location = "actors[" + std::to_string(index) + "]";
        it->passToShader(programId, location);
    }
    mainCamera.passToShader(programId, "camera");
    mainLight.passToShader(programId, "light");
}


void scene::updateDimensions(int programId, glm::vec2 aspectRatio) {
    mainCamera.aspectRatio = aspectRatio;
    mainCamera.updateAspectRatio(programId, "camera");
}