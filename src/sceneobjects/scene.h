#ifndef BELEG_SCENE_H
#define BELEG_SCENE_H

#include "object.h"
#include "list"
#include "camera.h"
#include "light.h"

class scene : shaderPassable {

public:
    std::list<object> actors;
    camera mainCamera;
    light mainLight;

    scene();
    scene(camera mainCamera, light mainLight);

    void passToShader(int programId);
    void updateDimensions(int programId, glm::vec2 aspectRatio);
};


#endif
