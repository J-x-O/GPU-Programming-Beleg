#ifndef BELEG_CAMERA_H
#define BELEG_CAMERA_H


#include <string>
#include <vec2.hpp>
#include <vec3.hpp>
#include <vec4.hpp>
#include "shaderPassable.h"

class camera  : shaderPassable {
public:
    glm::vec3 position;
    glm::vec3 direction;
    float projectionDistance;
    glm::vec2 aspectRatio;
    float scale;
    float startGridScale;
    glm::vec4 skyboxColor;

    camera();

    camera(glm::vec3 position, glm::vec3 direction, float projectionDistance, float scale, float startGridScale);

    camera(glm::vec3 position, glm::vec3 direction, float projectionDistance, glm::vec2 aspectRatio,
           float scale, float startGridScale);

    camera(glm::vec3 position, glm::vec3 direction, float projectionDistance, glm::vec2 aspectRatio,
           float scale, float startGridScale, glm::vec4 skyboxColor);

    void passToShader(int programId, const std::string identifier);
    void handleDirection(int programId, const std::string identifier);
    void updateAspectRatio(int programId, const std::string identifier);
};


#endif
