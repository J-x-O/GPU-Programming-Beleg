#ifndef BELEG_OBJECTMATERIAL_H
#define BELEG_OBJECTMATERIAL_H


#include <vec3.hpp>
#include <string>
#include "shaderPassable.h"

class objectMaterial : shaderPassable {

public:
    glm::vec3 color;
    float reflectivity;

    objectMaterial();
    objectMaterial(glm::vec3 color);
    objectMaterial(glm::vec3 color, float reflectivity);

    void passToShader(int programId, const std::string& identifier);
};


#endif
