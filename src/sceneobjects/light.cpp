#include <glad/glad.h>
#include "light.h"

light::light(glm::vec3 position) : light(position, glm::vec3(1,1,1)){}

light::light(glm::vec3 position, glm::vec3 color) : shaderPassable(), position(position), color(color) {}

void light::passToShader(int programId, const std::string& identifier) {

    std::string currentIdentifier;
    GLuint currentLocation;

    currentIdentifier = identifier + ".position";
    currentLocation = getUniformLocation(programId, currentIdentifier);
    glUniform3f(currentLocation, position.x, position.y, position.z);

    currentIdentifier = identifier + ".color";
    currentLocation = getUniformLocation(programId, currentIdentifier);
    glUniform3f(currentLocation, color.x, color.y, color.z);
}