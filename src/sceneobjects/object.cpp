#include "object.h"
#include <string>
#include <mat4x4.hpp>

object::object(shapeType type) : object(objectMaterial(), type) {}

object::object(objectMaterial material, shapeType type) : shaderPassable(),
        transform(objectTransform()), material(material), type(type) {}

void object::passToShader(int programId, const std::string& identifier) {

    std::string currentIdentifier;
    GLuint currentLocation;

    currentIdentifier = identifier + ".transform";
    GLuint location = getUniformLocation(programId, currentIdentifier);
    glm::mat4 translationMat = transform.calculateMatrix();
    glUniformMatrix4fv(location, 1, GL_FALSE, &translationMat[0][0]);

    currentIdentifier = identifier + ".dimension";
    currentLocation = getUniformLocation(programId, currentIdentifier);
    glUniform3f(currentLocation, transform.scale.x, transform.scale.y, transform.scale.z);

    currentIdentifier = identifier + ".material";
    material.passToShader(programId, currentIdentifier);

    currentIdentifier = identifier + ".shapeType";
    currentLocation = getUniformLocation(programId, currentIdentifier);
    glUniform1i(currentLocation, type);
}
