#ifndef BELEG_OBJECT_H
#define BELEG_OBJECT_H

#include <string>
#include <vec4.hpp>
#include <vec3.hpp>
#include <unordered_map>
#include <glad/glad.h>
#include "objectTransform.h"
#include "objectMaterial.h"
#include "shaderPassable.h"

enum shapeType { SPHERE = 0, CUBE = 1, TORUS = 2 };

class object : shaderPassable {

public:
    objectTransform transform;
    objectMaterial material;
    shapeType type;

    object(shapeType type);
    object(objectMaterial material, shapeType type);

    void passToShader(int programId, const std::string& identifier);
};

#endif
