#include <glad/glad.h>
#include "camera.h"

camera::camera() :
    camera(glm::vec3(0), glm::vec3(1,0,0), 1, 0.5, 0.9) {}

camera::camera(glm::vec3 position, glm::vec3 direction, float projectionDistance, float scale, float startGridScale) :
    camera(position, direction, projectionDistance, glm::vec2(16, 9), scale, startGridScale) {}

camera::camera(glm::vec3 position, glm::vec3 direction, float projectionDistance,
               glm::vec2 aspectRatio, float scale, float startGridScale) :
    camera(position, direction, projectionDistance,
    aspectRatio, scale, startGridScale, glm::vec4(0.25, 0.25, 50, 1)) {}

camera::camera(glm::vec3 position, glm::vec3 direction, float projectionDistance,
               glm::vec2 aspectRatio, float scale, float startGridScale, glm::vec4 skyboxColor) :
    shaderPassable(),
    position(position), direction(direction), projectionDistance(projectionDistance),
    aspectRatio(aspectRatio), scale(scale), startGridScale(startGridScale), skyboxColor(skyboxColor) {}

void camera::passToShader(int programId, const std::string identifier) {

    std::string currentIdentifier;
    GLuint currentLocation;

    currentIdentifier = identifier + ".position";
    currentLocation = getUniformLocation(programId, currentIdentifier);
    glUniform3f(currentLocation, position.x, position.y, position.z);

    handleDirection(programId, identifier);

    currentIdentifier = identifier + ".projectionDistance";
    currentLocation = getUniformLocation(programId, currentIdentifier);
    glUniform1f(currentLocation, projectionDistance);

    updateAspectRatio(programId, identifier);

    currentIdentifier = identifier + ".scale";
    currentLocation = getUniformLocation(programId, currentIdentifier);
    glUniform1f(currentLocation, scale);

    currentIdentifier = identifier + ".startGridScale";
    currentLocation = getUniformLocation(programId, currentIdentifier);
    glUniform1f(currentLocation, startGridScale);

    currentIdentifier = identifier + ".skyboxColor";
    currentLocation = getUniformLocation(programId, currentIdentifier);
    glUniform3f(currentLocation, skyboxColor.r, skyboxColor.g, skyboxColor.b);
}

void camera::handleDirection(int programId, const std::string identifier) {
    std::string currentIdentifier = identifier + ".direction";
    GLuint currentLocation = getUniformLocation(programId, currentIdentifier);
    glUniform3f(currentLocation, direction.x, direction.y, direction.z);
}

void camera::updateAspectRatio(int programId, const std::string identifier) {

    std::string currentIdentifier = identifier + ".aspectRatio";
    GLuint currentLocation = getUniformLocation(programId, currentIdentifier);
    glUniform2f(currentLocation, aspectRatio.x, aspectRatio.y);
}
