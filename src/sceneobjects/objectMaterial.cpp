#include <glad/glad.h>
#include "objectMaterial.h"

objectMaterial::objectMaterial() : objectMaterial(glm::vec3(0.5)) {}

objectMaterial::objectMaterial(glm::vec3 color) : objectMaterial(color, 0.1){}

objectMaterial::objectMaterial(glm::vec3 color, float reflectivity) :
    shaderPassable(), color(color), reflectivity(reflectivity) {}


void objectMaterial::passToShader(int programId, const std::string &identifier) {

    std::string currentIdentifier;
    GLuint currentLocation;

    currentIdentifier = identifier + ".color";
    currentLocation = getUniformLocation(programId, currentIdentifier);
    glUniform3f(currentLocation, color.x, color.y, color.z);

    currentIdentifier = identifier + ".reflectivity";
    currentLocation = getUniformLocation(programId, currentIdentifier);
    glUniform1f(currentLocation, reflectivity);
}

