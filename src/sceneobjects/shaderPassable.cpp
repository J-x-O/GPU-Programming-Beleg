#include "shaderPassable.h"

shaderPassable::shaderPassable() : uniformLocationCache() {}

//https://www.youtube.com/watch?v=nBB0LGSIm5Q
GLint shaderPassable::getUniformLocation(int programId, const std::string &identifier) {

    if (uniformLocationCache.find(programId) == uniformLocationCache.end())
        uniformLocationCache[programId] = std::unordered_map<std::string, GLint>();

    if (uniformLocationCache[programId].find(identifier) != uniformLocationCache[programId].end())
        return uniformLocationCache[programId][identifier];

    GLuint location = glGetUniformLocation(programId, identifier.c_str());
    uniformLocationCache[programId][identifier] = location;
    return location;
}
