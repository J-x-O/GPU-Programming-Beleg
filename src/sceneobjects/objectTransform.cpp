#include "glm.hpp"
#include "gtx/transform.hpp"
#include "objectTransform.h"
#include "mat4x4.hpp"
#include "vec3.hpp"

#define PI 3.141592f

objectTransform::objectTransform()
    : objectTransform(glm::vec3(0,0,0)) {}

objectTransform::objectTransform(glm::vec3 position)
    : objectTransform(position, glm::vec3(0,0,0)) {}

objectTransform::objectTransform(glm::vec3 position, glm::vec3 rotation)
    : objectTransform(position, rotation, glm::vec3(1,1,1)) {}

objectTransform::objectTransform(glm::vec3 position, glm::vec3 rotation, glm::vec3 scale)
    : position(position), rotation(rotation), scale(scale) {}

glm::mat4x4 objectTransform::calculateMatrix() {

    // http://www.opengl-tutorial.org/beginners-tutorials/tutorial-3-matrices/
    glm::mat4 translationMat = glm::translate(glm::mat4(1.0f), position);

    // https://learnopencv.com/rotation-matrix-to-euler-angles/
    glm::mat4 rotationMatX = glm::rotate(glm::mat4(1.0f), rotation.x / 180.0f * PI, glm::vec3(1, 0, 0));
    glm::mat4 rotationMatY = glm::rotate(glm::mat4(1.0f), rotation.y / 180.0f * PI, glm::vec3(0, 1, 0));
    glm::mat4 rotationMatZ = glm::rotate(glm::mat4(1.0f), rotation.z / 180.0f * PI, glm::vec3(0, 0, 1));
    glm::mat4 rotationMat = rotationMatZ * rotationMatY * rotationMatX;

    // Scale will not work this way, as implied by this article in the section about transforms:
    // https://iquilezles.org/www/articles/distfunctions/distfunctions.htm

    glm::mat4 finalMat = translationMat * rotationMat;

    return finalMat;
}
