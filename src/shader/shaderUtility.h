#ifndef BELEG_SHADERUTILITY_H
#define BELEG_SHADERUTILITY_H

#include <string>

/// A struct containing the read in source code for both shader types
struct shaderProgramSource {
    std::string VertexSource;
    std::string FragmentSource;
};

/// A utility function for reading in a shader from a file.
/// Uses '#shader vertex' and '#shader fragment', to seperate the types
/// \param filePath The path to the file that will be read in
/// \return The source code to both shader types as strings
shaderProgramSource parseShader(const std::string& filePath);

/// A utility function for compiling a shader
/// \param type The type of shader that will be compiled (GL_VERTEX_SHADER / GL_FRAGMENT_SHADER)
/// \param source A pointer to a string containing the source code of the Shader
/// \return The Id of the compiled Shader
unsigned int compileShader(unsigned int type, const std::string& source);

/// A utility function to create a shader program
/// \param vertexShader A pointer to a string containing the source Code of the Vertex Shader
/// \param fragmentShader A pointer to a string containing the source Code of the Fragment Shader
/// \return The Id of the program that is created
unsigned int createShader(const std::string& vertexShader, const std::string& fragmentShader);

#endif //BELEG_SHADERUTILITY_H
