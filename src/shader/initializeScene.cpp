#include "initializeScene.h"
#include <math.h>

scene initializeScene() {

    camera mainCamera = camera(glm::vec3(0, 3, -5), glm::vec3(0,-0.5,1),
                               0.5, 0.3f, 0);
    light mainLight = light(glm::vec3(0,2,0));

    scene createdScene = scene(mainCamera, mainLight);

    object floor = object(glm::vec3(5, 0.1f, 5), shapeType::CUBE);
    floor.transform.position = glm::vec3(0, -0.5f, 0);
    floor.transform.scale = glm::vec3(5, 0.1f, 5);
    floor.material.color = glm::vec4(0.5, 0.5f, 0.5f, 1);
    floor.material.reflectivity = 0.3;
    createdScene.actors.push_back(floor);

    object cube1 = (shapeType::CUBE);
    cube1.transform.position = glm::vec3(2, 0, 0);
    cube1.transform.scale = glm::vec3(0.5f);
    cube1.material.color = glm::vec4(1, 0.2f, 0.2f, 1);
    cube1.material.reflectivity = 0.2;
    createdScene.actors.push_back(cube1);

    object sphere1 = (shapeType::SPHERE);
    sphere1.transform.position = glm::vec3(-2, 0, 0);
    sphere1.transform.scale = glm::vec3(0.5f);
    sphere1.material.color = glm::vec4(0.2f,1,0.2f,1);
    sphere1.material.reflectivity = 0.8;
    createdScene.actors.push_back(sphere1);

    object cylinder1 = (shapeType::TORUS);
    cylinder1.transform.position = glm::vec3(0, 0.3, 0);
    cylinder1.transform.rotation = glm::vec3(90, 90, 0);
    cylinder1.transform.scale = glm::vec3(0.5f, 0.2, 0.5);
    cylinder1.material.color = glm::vec4(0.2f,0.2f,1,1);
    cylinder1.material.reflectivity = 0.8;
    createdScene.actors.push_back(cylinder1);

    return createdScene;
}