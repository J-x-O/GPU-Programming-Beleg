#ifndef BELEG_RAYMARCHINGSETUP_H
#define BELEG_RAYMARCHINGSETUP_H

#include "shaderUtility.h"
#include "initializeScene.h"
#include <iostream>
#include <glad/glad.h>

// for some reason putting the variables here resulted in linking errors
scene* getCurrentScene();
unsigned int getCurrentProgram();

/// Needs to be called before the first Frame is being rendered.
/// Creates Shaders, the Program and SetsUp all variables;
void setupShader(int width, int height);

/// Needs to be called every Frame to calculate and draw the RayMarchingResult
void runRaymarchingShader(int width, int height, bool advanceBudgetTime);

#endif //BELEG_RAYMARCHINGSETUP_H
