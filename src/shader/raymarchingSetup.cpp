#include "raymarchingSetup.h"

unsigned int programID;
scene mainScene;

// define getter
scene* getCurrentScene() { return &mainScene;}
unsigned int getCurrentProgram() { return programID;}


void GLAPIENTRY MessageCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam ) {
    fprintf( stderr, "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
             ( type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : "" ),
             type, severity, message);
}

void setupShader(int width, int height) {
    glEnable(GL_DEBUG_OUTPUT);
    glDebugMessageCallback(MessageCallback, nullptr);

    // positions for two triangles over the whole screen
    float positions[] {
            -1.0f, -1.0f,
            1.0f, -1.0f,
            1.0f,  1.0,

            1.0f,  1.0f,
            -1.0f,  1.0f,
            -1.0f, -1.0f,
    };

    // Link the Buffer with all availiable Data
    unsigned int buffer;
    glGenBuffers(1, &buffer);
    glBindBuffer(GL_ARRAY_BUFFER, buffer);
    glBufferData(GL_ARRAY_BUFFER, 6 * 2 * sizeof(float), positions, GL_STATIC_DRAW);

    // SetUp Position Attribute
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), 0);
    glEnableVertexAttribArray(0);

    // Compile the Shader with packup path
    shaderProgramSource source = parseShader("src/shader/glsl/raymarching.shader");
    if(source.VertexSource.empty() && source.FragmentSource.empty())
        source = parseShader("../src/shader/glsl/raymarching.shader");
    programID = createShader(source.VertexSource, source.FragmentSource);
    glUseProgram(programID);

    mainScene = initializeScene();
    mainScene.passToShader(programID);
}

float budgetTime = 0;

void runRaymarchingShader(int width, int height, bool advanceBudgetTime) {

    if(advanceBudgetTime) budgetTime += 0.02f;

    // Update dimensions attributes
    mainScene.updateDimensions(programID, glm::vec2(width, height));

    mainScene.mainCamera.position = glm::vec3(sinf(budgetTime / 3) * 5, 1, cosf(budgetTime / 3) * 5);
    mainScene.mainCamera.direction = glm::vec3() - mainScene.mainCamera.position;

    if(!mainScene.actors.empty()) {
        object target = mainScene.actors.front();
        target.transform.rotation = glm::vec3(budgetTime, 0, budgetTime);
        target.transform.scale = glm::vec3(sin(budgetTime) / 4 + 0.5f);
    }

    mainScene.passToShader(programID);

    // Draw the scene
    glDrawArrays(GL_TRIANGLES, 0, 6);
}

