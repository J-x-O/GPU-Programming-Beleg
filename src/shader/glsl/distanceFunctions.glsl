// Operations

float SphereDistance(vec3 from, float radius) {
    return length(from) - radius;
}

// https://www.youtube.com/watch?v=62-pRVZuS5c
float CubeDistance(vec3 from, vec3 scale) {
    vec3 value = abs(from)-scale;

    // this should be the same as value = max(value, 0), but for some reason any setup
    // that doesn't has at least one of the if statements instead of a max (for either x, y or z)
    // produces weird artefacts, where the cube extends into infinity into each direction :/
    if(value.x < 0.) { value.x = 0.; }
    if(value.y < 0.) { value.y = 0.; }
    if(value.z < 0.) { value.z = 0.; }

    return length(value) - min(max(value.x, max(value.y,value.z)),0.0);

    // original code
    vec3 q = abs(from) - scale;
    return length(max(q,0.0)) - min(max(q.x, max(q.y,q.z)),0.0);
}

float TorusDistance(vec3 from, float innerRadius, float outerRadius){
    vec2 q = vec2(length(from.xz) - innerRadius, from.y);
    return length(q) - outerRadius;
}


