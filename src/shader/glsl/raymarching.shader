// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
//                                                                                                       //
//                                              Vertex Shader                                            //
//                                                                                                       //
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //

#shader vertex
#version 460

layout(location = 0) in vec4 position;

// Output block to pass data to the fragment shader
// The Position will be interpolated between the verts
out VertexData {
    vec4 position;
}outData;

// Apply the Position to the Vertex and pass on the Data
void main() {
    gl_Position = position;
    outData.position = position;
}

// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
//                                                                                                       //
//                                             Fragment Shader                                           //
//                                                                                                       //
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //

#shader fragment
#version 460

// Static Variables
#define FLT_MAX 3.402823466e+38
#define M_PI 3.1415926535897932384626433832795
#define ACTOR_LIMIT 64
#define SEARCH_LIMIT 128
#define DISTANCE_THRESHHOLD 0.001f
#define GLOW_STRENGTH 50
#define AMBIENT_OCCLUSION_STEPSIZE 0.05f
#define AMBIENT_OCCLUSION_INTENSITY 0.3f
#define AMBIENT_OCCLUSION_ITERATIONS 5
#define SOFTSHADOW_MULTIPLIER 2
#define REFLECTION_LIMIT 1

// Shader Options
//#define DEBUGMODE
//#define AMBIENT_OCCLUSION_DEBUGVIEW
#define GLOW_OUTLINE
#define AMBIENT_OCCLUSION
#define SOFTSHADOWS
#define SOFTSHADOW_CLAMP
#define BETTER_SOFTSHADOW_METHOD
#define REFLECTIONS

#include distanceFunctions.glsl

// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
//                                                                                                       //
//                                               Definitions                                             //
//                                                                                                       //
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //

// A struct containing all Data for the Cam
struct CameraData {
    vec3 position;
    vec3 direction;
    float projectionDistance;
    vec2 aspectRatio;
    float scale;
    float startGridScale;
    vec3 skyboxColor;
};

struct Light {
    vec3 position;
    vec3 color;
};

struct Material {
    vec3 color;
    float reflectivity;
};

// A struct defining a sceneObject with all of its properties
struct Actor {
    mat4 transform;
    vec3 dimension;
    Material material;
    int shapeType;
    int operation;
};

// Input from the Fragment Shader
in VertexData {
    vec4 position;
}inData;

// External Variables, that are passed in
uniform float actorCount;
uniform Actor[ACTOR_LIMIT] actors;
uniform CameraData camera;
uniform Light light;

// Output Attribute Color
out vec3 color;

struct Ray {
    vec3 startPosition;
    vec3 direction;
};

struct ClosestObject {
    int index;
    float distance;
};

struct RayCastResult {
    bool hitSomething;
    vec3 currentPosition;
    float minDistance;
    int stepCount;
    ClosestObject object;
};

// Rays / Coloring
Ray CalculateCameraRay();
RayCastResult CastRay(Ray ray);
vec3 CastShadowRay(vec3 startPosition);
float AmbientOcclusion(vec3 from, vec3 normal);
vec3 SkyBoxColor(vec3 direction, float lowestDistance);

// Distance Estimation
ClosestObject FindClosestObject(vec3 currentPosition);
float CalculateDistance(int index, vec3 from);
vec3 CalculateNormal(int index, vec3 from);

// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
//                                                                                                       //
//                                                Functions                                              //
//                                                                                                       //
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //

void main() {

    #ifdef DEBUGMODE
    RayCastResult hit = CastRay(CalculateCameraRay());
    if(hit.object.distance < DISTANCE_THRESHHOLD) {
        float toFarIn = (DISTANCE_THRESHHOLD - hit.object.distance) / DISTANCE_THRESHHOLD;
        color = vec3(toFarIn, 0, 0);
        return;
    }
    #endif

    color = vec3(0,0,0);
    float colorStrength = 1;

    Ray ray = CalculateCameraRay();

    // we dont want to show the reflections
    #if !defined(REFLECTIONS) || defined(AMBIENT_OCCLUSION_DEBUGVIEW)
    int reflectionLimit = 0;
    #else
    int reflectionLimit = REFLECTION_LIMIT;
    #endif

    for(int i = 0; i <= reflectionLimit; i++) {

        RayCastResult hit = CastRay(ray);

        if(hit.hitSomething) {
            vec3 normal = CalculateNormal(hit.object.index, hit.currentPosition);
            vec3 correctedPosition = hit.currentPosition + normal * DISTANCE_THRESHHOLD * 3;

            // base color with shading
            Material target = actors[hit.object.index].material;
            vec3 hitColor = target.color * (CastShadowRay(correctedPosition) * 0.5 + 0.5);

            // Ambient Occlusion
            #ifdef AMBIENT_OCCLUSION
            #ifdef AMBIENT_OCCLUSION_DEBUGVIEW
            hitColor = vec3(0.95f);
            #endif
            hitColor *= AmbientOcclusion(hit.currentPosition, normal);
            #endif

            // Dont take in the reflectionstrength if we dont have reflections
            float reflectionStrength = target.reflectivity;
            if(reflectionLimit == 0) reflectionStrength = 0;

            // color is made up of all hits with sinking impact
            color += hitColor * colorStrength * (1 - reflectionStrength);
            colorStrength *= reflectionStrength;

            //If our colorStrength is too low, we can exit to save performace
            if(colorStrength < 0.001f) break;

            // setup next round
            ray = Ray(correctedPosition, reflect(ray.direction, normal));
        }
        else {
            float lowestDistance = hit.minDistance;
            if(i != 0) lowestDistance = FLT_MAX; // lowest Distance isn't accurate if we start from a surface
            color += SkyBoxColor(ray.direction, lowestDistance) * colorStrength;
            return;
        }
    }
}

// https://www.youtube.com/watch?v=22PZF7fWLqI
float AmbientOcclusion(vec3 from, vec3 normal) {
    float result = 0.0f;

    for(int i = 1; i <= AMBIENT_OCCLUSION_ITERATIONS; i++) {
        float dist = AMBIENT_OCCLUSION_STEPSIZE * i;
        float sampleValue = FindClosestObject(from + normal * dist).distance;
        result += max(0, dist - sampleValue / dist);
    }

    return 1 - result * AMBIENT_OCCLUSION_INTENSITY;
}

vec3 SkyBoxColor(vec3 direction, float lowestDistance) {

    //nothing was hit, we reached the search limit
    vec3 color = camera.skyboxColor;

    // Skybox fade (up brighter, down darker)
    vec2 direction2D = vec2(length(direction.xz), direction.y);
    float dotResult = dot(direction2D, vec2(0, 1));
    color += dotResult / 4;

    // GlowOutline
    #ifdef GLOW_OUTLINE
    color += 1 / (lowestDistance * GLOW_STRENGTH);
    #endif

    #ifdef AMBIENT_OCCLUSION_DEBUGVIEW
    return vec3(0.5);
    #endif

    return color;
}

// -------------------------------------------------- Rays --------------------------------------------------

Ray CalculateCameraRay() {
    vec3 right = normalize(cross(vec3(0,1,0), camera.direction));
    vec3 up = normalize(cross(camera.direction, right));

    float aspectRatioWidth = camera.aspectRatio.x / camera.aspectRatio.y;

    // TargetGrid
    vec3 targetRightOffset = right * aspectRatioWidth * camera.scale * inData.position.x;
    vec3 targetUpOffset = up * camera.scale *  inData.position.y;
    vec3 projectionOffset =  normalize(camera.direction) * camera.projectionDistance;
    vec3 targetPosition = camera.position + projectionOffset + targetRightOffset + targetUpOffset;

    // StartGrid
    vec3 startRightOffset = targetRightOffset * camera.startGridScale;
    vec3 startUpOffset = targetUpOffset * camera.startGridScale;
    vec3 startPosition = camera.position + startRightOffset + startUpOffset;

    // Ray goes from startgrid to the targetGrid at the corresponding Position
    // Rather primitive, but easy approach to a Camera
    vec3 rayDirection = normalize(targetPosition - startPosition);

    return Ray(startPosition, rayDirection);
}

RayCastResult CastRay(Ray ray) {

    vec3 color = vec3(0,0,0);
    vec3 currentPosition = ray.startPosition;
    float lowestDistance = FLT_MAX;

    for(int i = 0; i < SEARCH_LIMIT; i++) {

        ClosestObject closestObject = FindClosestObject(currentPosition);
        lowestDistance = min(lowestDistance, closestObject.distance);

        // Hit Registered
        if(closestObject.distance <= DISTANCE_THRESHHOLD) {
            return RayCastResult(true, currentPosition, lowestDistance, i, closestObject);
        }

        // March as far as we can towards our direction
        currentPosition = currentPosition + ray.direction * closestObject.distance;
    }

    return RayCastResult(false, currentPosition, lowestDistance, SEARCH_LIMIT, ClosestObject(-1, -1));
}

vec3 CastShadowRay(vec3 startPosition) {

    vec3 direction = normalize(light.position - startPosition);
    float totalDistanceToLight = distance(startPosition, light.position);
    float lightValue = FLT_MAX;

    float previousDistance;

    float progress = 0f;
    for(int i = 0; i < SEARCH_LIMIT; i++) {

        float currentDistance = FindClosestObject(startPosition + direction * progress).distance;

        // https://www.iquilezles.org/www/articles/rmshadows/rmshadows.htm

        #ifdef SOFTSHADOWS
        #ifdef BETTER_SOFTSHADOW_METHOD
        float distanceToInbetweenPoint = currentDistance * currentDistance / (2.0 * previousDistance);
        float inbetweenDistance = sqrt(currentDistance * currentDistance - distanceToInbetweenPoint * distanceToInbetweenPoint);
        lightValue = min(lightValue, SOFTSHADOW_MULTIPLIER  * inbetweenDistance / max(0.0f, progress - distanceToInbetweenPoint));
        #else
        lightValue = min(lightValue, SOFTSHADOW_MULTIPLIER  * currentDistance / progress);
        #endif
        #endif

        // Hit Registered
        if (currentDistance < DISTANCE_THRESHHOLD) break;

        // March as far as we can towards the light
        previousDistance = currentDistance;
        progress += currentDistance;

        if(progress > totalDistanceToLight) {
            #ifdef SOFTSHADOWS
            #ifdef SOFTSHADOW_CLAMP
            lightValue = clamp(lightValue, 0.0f, 1.0f);
            #endif
            return light.color * lightValue;
            #else
            return light.color;
            #endif
        };
    }
    return vec3(0);
}

// ------------------------------------- Distance Estimation / Normals -------------------------------------

ClosestObject FindClosestObject(vec3 currentPosition) {

    ClosestObject closestObject = ClosestObject(-1, FLT_MAX);
    for(int i = 0; i < actorCount; i++) {

        float distance = CalculateDistance(i, currentPosition);

        if(distance <= closestObject.distance) {
            closestObject.distance = distance;
            closestObject.index = i;
        }
    }
    return closestObject;
}

float CalculateDistance(int index, vec3 from) {
    vec4 transformedPosition = inverse(actors[index].transform) * vec4(from, 1);
    Actor target = actors[index];
    switch (target.shapeType) {
        case 0: return SphereDistance(transformedPosition.xyz, target.dimension.x);
        case 1: return CubeDistance(transformedPosition.xyz, target.dimension);
        case 2: return TorusDistance(transformedPosition.xyz, target.dimension.x, target.dimension.y);
        default: return FLT_MAX;
    }
}

// https://www.iquilezles.org/www/articles/normalsSDF/normalsSDF.htm
vec3 CalculateNormal(int index, vec3 from) {
    const float h = 0.0001; // replace by an appropriate value
    const vec2 k = vec2(1,-1);
    return normalize( k.xyy * FindClosestObject(from + k.xyy * h ).distance +
    k.yyx * FindClosestObject(from + k.yyx * h ).distance +
    k.yxy * FindClosestObject(from + k.yxy * h ).distance +
    k.xxx * FindClosestObject(from + k.xxx * h ).distance );
}