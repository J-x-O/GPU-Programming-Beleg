#include "shaderUtility.h"
#include <glad/glad.h>

#include <iostream>
#include <fstream>
#include <sstream>

std::string parseSimpleShader(const std::string& filePath) {
    std::ifstream stream(filePath);
    std::string line;
    std::stringstream buffer;
    while(getline(stream, line)) {
        buffer << line << '\n';
    }
    return buffer.str();
}


shaderProgramSource parseShader(const std::string& filePath) {
    std::ifstream stream(filePath);

    enum ShaderType { NONE = -1, VERTEX = 0, FRAGMENT = 1};

    std::string line;
    std::stringstream shaderBuffer[2];
    ShaderType type = ShaderType::NONE;
    while(getline(stream, line)) {

        // Shader definition
        if(line.find("#shader") != std::string::npos) {
            if(line.find("vertex") != std::string::npos)
                type = ShaderType::VERTEX;
            if(line.find("fragment") != std::string::npos)
                type = ShaderType::FRAGMENT;
        }
        // custom includes
        else if(line.find("#include") != std::string::npos) {
            std::string relFilePath = line.substr(line.find("#include") + 9);
            std::string currentFilePath = filePath.substr(0, filePath.find_last_of("/") + 1);
            std::string includedString;
            try {
                includedString = parseSimpleShader(currentFilePath.append(relFilePath));
                shaderBuffer[type] << includedString;
            }
            catch (...) {
                throw "Couln't resolve #include path";
            }

        }

        // Add it to the right buffer
        else {
            if(type == ShaderType::NONE) continue;
            shaderBuffer[type] << line << '\n';
        }
    }
    return { shaderBuffer[0].str(), shaderBuffer[1].str() };
}

unsigned int compileShader(unsigned int type, const std::string& source) {

    // Create and compile the shader
    unsigned int id = glCreateShader(type);
    const char* src = source.c_str();
    glShaderSource(id, 1, &src, nullptr);
    glCompileShader(id);

    // Error Handling
    int result;
    glGetShaderiv(id, GL_COMPILE_STATUS, &result);
    if(result == GL_FALSE) {
        int length;
        glGetShaderiv(id, GL_INFO_LOG_LENGTH, &length);
        char* message = (char*)alloca(length * sizeof(char));
        glGetShaderInfoLog(id, length, &length, message);
        std::cout << "Failed to compile " <<
                (type == GL_VERTEX_SHADER ? "vertex" : "fragment")
                << " Shader!" << std::endl;
        std::cout << message << std::endl;
        glDeleteShader(id);
        return 0;
    }

    return id;
}

// Error Handling
bool checkProgramStatus(unsigned int id) {
    int result;
    glGetProgramiv(id, GL_LINK_STATUS, &result);
    if(result == GL_FALSE) {
        int length;
        glGetProgramiv(id, GL_INFO_LOG_LENGTH, &length);
        char* message = (char*)alloca(length * sizeof(char));
        glGetProgramInfoLog(id, length, &length, message);
        std::cout << "Failed to link Shader!" << std::endl;
        std::cout << message << std::endl;
        glDeleteShader(id);
        return false;
    }
    return true;
}

unsigned int createShader(const std::string& vertexShader, const std::string& fragmentShader) {
    unsigned int program = glCreateProgram();
    unsigned int vs = compileShader(GL_VERTEX_SHADER, vertexShader);
    unsigned int fs = compileShader(GL_FRAGMENT_SHADER, fragmentShader);

    glAttachShader(program, vs);
    glAttachShader(program, fs);
    glLinkProgram(program);
    checkProgramStatus(program);
    glValidateProgram(program);

    glDeleteShader(vs);
    glDeleteShader(fs);

    return program;
}


