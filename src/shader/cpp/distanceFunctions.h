#ifndef BELEG_DISTANCEFUNCTIONS_H
#define BELEG_DISTANCEFUNCTIONS_H

#include <vec3.hpp>

float sphereDistance(glm::vec3 from, float radius);

float cubeDistance(glm::vec3 from, glm::vec3 scale);

float torusDistance(glm::vec3 from, float height, float radius);

#endif
