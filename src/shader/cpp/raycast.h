#ifndef BELEG_RAYCAST_H
#define BELEG_RAYCAST_H

#include <vec3.hpp>
#include "../../sceneobjects/scene.h"
#include "distanceFunctions.h"

#define SEARCH_LIMIT 512
#define DISTANCE_THRESHHOLD 0.001f

struct raycastResult {
    bool hitSomething;
    glm::vec3 hitPosition;

    raycastResult() : hitSomething(false), hitPosition() {}
    raycastResult(glm::vec3 hitPosition) : hitSomething(true), hitPosition(hitPosition) {}
};

raycastResult raycast(scene* target, glm::vec2 screenPos);


#endif
