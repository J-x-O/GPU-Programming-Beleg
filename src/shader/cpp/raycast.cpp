#include <geometric.hpp>
#include <matrix.hpp>
#include "raycast.h"

struct ray {
    glm::vec3 startPosition;
    glm::vec3 direction;

    ray(const glm::vec3 &startPosition, glm::vec3 direction) : startPosition(startPosition), direction(direction) {}
};
ray calculateRay(scene* target, glm::vec2 screenPos);

struct closestObject {
    object* hit;
    float distance;

    closestObject(object* hit, float distance) : hit(hit), distance(distance) {}
};
closestObject findClosestObject(scene* target, glm::vec3 currentPosition);

raycastResult raycast(scene* target, glm::vec2 screenPos) {
    ray ray = calculateRay(target, screenPos);

    glm::vec3 currentPosition = ray.startPosition;
    float lowestDistance = FLT_MAX;
    for(int i = 0; i < SEARCH_LIMIT; i++) {
        closestObject closestObject = findClosestObject(target, currentPosition);
        lowestDistance = std::min(lowestDistance, closestObject.distance);

        // Hit Registered
        if(closestObject.distance <= DISTANCE_THRESHHOLD) {
            return raycastResult(currentPosition);
        }

        // March as far as we can towards our direction
        currentPosition = currentPosition + ray.direction * closestObject.distance;
    }
    return raycastResult();
}

ray calculateRay(scene* target, glm::vec2 screenPos) {
    camera camera = target->mainCamera;

    glm::vec3 right = normalize(glm::cross(glm::vec3(0,1,0), camera.direction));
    glm::vec3 up = normalize(cross(camera.direction, right));

    float aspectRatioWidth = camera.aspectRatio.x / camera.aspectRatio.y;

    // TargetGrid
    glm::vec3 targetRightOffset = right * aspectRatioWidth * camera.scale * screenPos.x;
    glm::vec3 targetUpOffset = up * camera.scale *  screenPos.y;
    glm::vec3 projectionOffset =  normalize(camera.direction) * camera.projectionDistance;
    glm::vec3 targetPosition = camera.position + projectionOffset + targetRightOffset + targetUpOffset;

    // StartGrid
    glm::vec3 startRightOffset = targetRightOffset * camera.startGridScale;
    glm::vec3 startUpOffset = targetUpOffset * camera.startGridScale;
    glm::vec3 startPosition = camera.position + startRightOffset + startUpOffset;

    // ray goes from startgrid to the targetGrid at the corresponding Position
    // Rather primitive, but easy approach to a Camera
    glm::vec3 rayDirection = normalize(targetPosition - startPosition);

    return ray(startPosition, rayDirection);
}

closestObject findClosestObject(scene* target, glm::vec3 currentPosition) {

    closestObject result = closestObject(nullptr, FLT_MAX);
    for(std::list<object>::iterator it = target->actors.begin(); it != target->actors.end(); it++) {
        float distance = FLT_MAX;
        glm::vec4 transformedPosition = glm::inverse(it->transform.calculateMatrix()) * glm::vec4(currentPosition, 1);
        switch (it->type) {
            case shapeType::SPHERE : distance = sphereDistance(transformedPosition, it->transform.scale.x); break;
            case shapeType::CUBE : distance = cubeDistance(transformedPosition, it->transform.scale); break;
            case shapeType::TORUS : distance = torusDistance(transformedPosition, it->transform.scale.y,
                                                             it->transform.scale.x); break;
        }
        if(distance <= result.distance) {
            result.distance = distance;
            result.hit = &*it;
        }
    }
    return result;
}
