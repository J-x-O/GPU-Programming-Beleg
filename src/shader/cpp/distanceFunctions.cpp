#include <common.hpp>
#include <geometric.hpp>
#include <vec2.hpp>
#include "distanceFunctions.h"

// Operations

float sphereDistance(glm::vec3 from, float radius) {
    return length(from) - radius;
}

// https://www.youtube.com/watch?v=62-pRVZuS5c
float cubeDistance(glm::vec3 from, glm::vec3 scale ) {
    glm::vec3 q = glm::abs(from) - scale;
    return glm::length(glm::max(q,glm::vec3(0.0))) +
        std::fmin(std::fmax(q.x, std::fmax(q.y,q.z)), 0.0);
}

float torusDistance(glm::vec3 from, float height, float radius) {
    glm::vec2 d = abs(glm::vec2(length(glm::vec2(from.x, from.y)), from.y)) - glm::vec2(height, radius);
    return std::fmin(std::fmax(d.x, d.y), 0.0) + length(glm::max(d, glm::vec2(0.0)));
}
